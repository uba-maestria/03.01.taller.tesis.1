# Trabajo de integración

Objetivo: TP de integración que sirve para la finalizacion de la especialización, sentar las bases para la tesis del siguiente año.

* Links más interesantes -> "argentinian analytics in 2015 elections"
	* Does Social Media cause Polarization? Evidence from access to Twitter Echo Chambers during the 2019 Argentine Presidential Debate: https://ideas.repec.org/p/nbr/nberwo/29458.html
	* Time to #Protest: Polarization and Time-to-Retweet in Argentina: https://www.researchgate.net/publication/313024375_Time_to_Protest_Polarization_and_Time-to-Retweet_in_Argentina
	* Representativeness of Abortion Legislation Debate on Twitter: A Case Study in Argentina and Chile https://labtomarket.files.wordpress.com/2020/04/fates2020.pdf

## Tema

* Redes sociales
* Polarizacion
* Echo chamber effect
* Elections
* Redes que se concentran // algunos puentes (Tipos de comunidades que se generan y tipo de red que se genera)


### Tareas

* Definir pregunta
   * Hay polarizacion? Hay preactivación? Hay condiciones de polarizacion de cara a las elecciones de 2022 en brasil? Hay comunidades? Que tantas conexiones hay entre usuarios con ideas diferentes? Hay echo chambers?

* **Check**. Escribir para Juan para pedirle la referencia del pibe
   * Son Tomás Cicchini y Pablo Balenzuela

* Scrapping
   * Elegir hashtags. Por ahora generales + los que son trendding topic relacionados.
   * Ver personalidades? DEberia hacer una lista de personas a las que traer los tweets, reacciones y retweets?
   * **Check**. Scrapping de twitter. Van 3 vueltas de scrapping. 30K tweets.

* Ordenar datos, limpiar y curar
   * Pasarlos a una base relacional? Creo que no vale la pena. Guardar como .pickle.
   * Select columns
   * Explore colums
   * Texto de los tweets, limpiarlo?

* Definir metodología, y métodos
   * Grafos
   * Comunidad
   * Canasta de items
   * Cluster
      * Kmeans
      * T-SNE
      * Encodings?
      
* Encontrar cuentas brasileñas
* Clasificarlas / agruparlas


## Links

Busqueda rápida 

Español

   * https://www.google.com.ar/search?q=twitter+polatization+argentina+analytics&ei=qpc7YrryF4mG1sQPusm3uAk&start=10&sa=N&ved=2ahUKEwi67-73nN32AhUJg5UCHbrkDZcQ8NMDegQIARBT&biw=1920&bih=911&dpr=1
   * http://www.scielo.org.ar/scielo.php?pid=S1853-19702020000200280&script=sci_arttext&tlng=es
   * https://ideas.repec.org/p/nbr/nberwo/29458.html
   * https://www.researchgate.net/publication/313024375_Time_to_Protest_Polarization_and_Time-to-Retweet_in_Argentina
   * https://labtomarket.files.wordpress.com/2020/04/fates2020.pdf
   * https://www.washingtonpost.com/news/monkey-cage/wp/2015/08/20/heres-what-you-need-to-know-about-argentinas-2015-federal-elections/
   * https://www.theguardian.com/world/2015/oct/23/argentina-elections-2015-a-guide-to-the-parties-polls-and-electoral-system
   * https://ourdataourselves.tacticaltech.org/posts/overview-argentina/
   * https://www.batimes.com.ar/news/argentina/argentina-cambiemos-and-cambridge-analytica.phtml
   * https://www.bakerinstitute.org/files/10888/
   * https://www.kcore-analytics.com/argentina-presidential-election/
   * https://www.researchgate.net/publication/336796985_Artificial_intelligence_for_elections_the_case_of_2019_Argentina_primary_and_presidential_election

Inglés

   * https://www.google.com.ar/search?q=elecciones+2015+polatizacion+twitter+analytics&ei=L5U7YsHkK9-r1sQPv6Kd4A0&ved=0ahUKEwiBuJ3Jmt32AhXflZUCHT9RB9wQ4dUDCA8&uact=5&oq=elecciones+2015+polatizacion+twitter+analytics&gs_lcp=Cgdnd3Mtd2l6EAM6BwgAEEcQsAM6CAguELEDEIMBOg4ILhCABBCxAxDHARCjAjoLCAAQgAQQsQMQgwE6BAguEEM6BAgAEEM6CggAELEDEIMBEEM6BwgAELEDEEM6CAgAEIAEELEDOgUIABCABDoGCAAQFhAeOgkIABDJAxAWEB46BQghEKABOgUIABCiBDoHCCEQChCgAToECCEQFUoECEEYAEoECEYYAFDmBljeX2DoYGgEcAF4AIABoQOIAdgjkgEJMzUuMTEuNC0xmAEAoAEByAEIwAEB&sclient=gws-wiz
   * http://www.scielo.org.ar/scielo.php?pid=S1853-19702020000200280&script=sci_arttext&tlng=es
   * https://riuma.uma.es/xmlui/bitstream/handle/10630/15389/TD_BUSTOS_DIAZ_Javier.pdf?sequence=1&isAllowed=y
   * https://www.memoria.fahce.unlp.edu.ar/tesis/te.1916/te.1916.pdf
   * http://sedici.unlp.edu.ar/handle/10915/118461
   * https://revistas.ufro.cl/ojs/index.php/perspectivas/article/view/2411
   * https://www.sciencedirect.com/science/article/pii/S187073001500006X
   * https://repositoriosdigitales.mincyt.gob.ar/vufind/Record/SEDICI_9fe5504372fa9671ed972efafe2364b0
   * https://eprints.ucm.es/id/eprint/49515/1/T40361.pdf
   * https://repositorio.udesa.edu.ar/jspui/bitstream/10908/15873/1/%5BP%5D%5BW%5D%20T.M.%20Per.%20Bay%C3%B3n%2C%20Carolina.pdf

