# Framing. La perspectiva de las noticias

ref: file:///run/media/lucas/Datos/Maestria/03.01.Taller.Tesis.1/03.01.taller.tesis.1/TP/Bibliografia/Aruguete_natalia-Framing_la_perspectiva_de_las_noticias.pdf


Sumario: Revisión teórica de la Teoría del Framing o Teoría del Encuadre

## Introducción

Desde inicios del siglo XX
La relación entre medios y opinión pública. 

1. "efectos poderosos" de una prensa activa sobre una masa pasiva,
2. "efectos limitados" de los medios, incapaces de generar cambios en opiniones y actitudes, hacia mediados de siglo. Hasta llegar, e
3. En los años ‘60 y ’70, efectos poderosos bajo condiciones limitadas. 
   * La teoría de la Agenda-Setting ubicó "la influencia fundamental en el interior del individuo y (estratificó) el contenido mediático según la compatibilidad que (tuviera) con las actitudes y opiniones preexistentes del individuo" (MCCOMBS, 2006, pp. 31 y 32).
4. Profundización de las críticas hacia los "efectos limitados" de los medios. 
   * Además fueron un desafío a los postulados iniciales de la Agenda-Setting. 

Así llegamos a Framing o Teoría del Encuadre.

Framing -> En 1955, el antropólogo Gregory Bateson (1972) propuso el concepto de "marco" como una herramienta que permitía explicar por qué la gente centra su atención en determinados aspectos de la realidad y no en otros. 

Goffman ->  "Las definiciones de una situación se forjan de acuerdo con principios de organización que gobiernan los eventos —al menos, los sociales— y nuestra relación subjetiva en ellos"

Heider -> Desde la teoría de la Atribución (1930) entendió que los seres humanos no logran entender la complejidad del mundo en que viven y tratan, por tanto, de inferir sus relaciones causales.

Gitlin (1980) introdujo el término frame en los estudios de comunicación

## Definición de Framing

Para **Entman (1993)**, los encuadres noticiosos ofrecen perspectivas sobre el asunto tratado para interpretar la información. Encuadrar es 

[…] seleccionar algunos aspectos de una realidad que se percibe y darles más relevancia en un texto comunicativo, de manera que se promueva una definición del problema determinado, una interpretación causal, una evaluación moral y/o una recomendación de tratamiento para el asunto descripto (p. 52). (Traducción propia) 

Los frames son herramientas fundamentales para transmitir informaciones: aumentan las perspectivas, revelan entendimientos particulares sobre los eventos y terminan transformando la forma de pensar del público sobre un asunto

**Reese** aportó una de las definiciones más completas de frame. "Son principios organizadores socialmente compartidos y persistentes en el tiempo, que trabajan simbólicamente para estructurar el mundo social de modo significativo" (REESE, 2001, p. 11, énfasis en original). (Traducción propia)


Entman desarrolló el rasgo interactivo del proceso de framing. Los esquemas de conocimiento de los periodistas los guían en su decisión sobre qué comunicar. Los encuadres de los textos ponen de manifiesto la ausencia o presencia de ciertas palabras clave, fuentes de información o imágenes. Los receptores tienen esquemas propios, que pueden o no coincidir con los de los periodistas y los textos

