# 03.01.Taller.Tesis.1

## Zoom

Taller de Tesis I - Martes de 19 a 22 hs.
https://exactas-uba.zoom.us/j/88687076390
ID de reunión: 886 8707 6390
Código de acceso: tti22-vcv

## Docentes

* Ricardo Maronna
	* Vertical virtual.
	* Estadístico
* Gustavo Juantorena
	* Primera vez dando clases en la maestría Guillermo. 
	* Profesor de exactas, en la carrera de ciencias de datos. Estudio física. Investigación en ciencias cognitivas.
* Guillermo Astorei
	* Haciendo el doctorado con Juan. En habla también, neurociencia, IA.
* Jazmin Vidal
	* Linguista, haciendo doctorado en exactas. Trabaja en procesamiento del habla.


Dos pares de profes, dos 'modalidades':

* Una más desatendida, solo interactuando con los profesores
* Una más como materia, cursando, entregando


## Modalidad

* Plan de tesis. Objetivos, background, introduccion, antecendente.
* TP integrando por lo menos dos materias. Para la especialización. 
* Taller de tesis II se cursa en ambos cuatrimestes. Probablemente el año que viene siga así.
	* Requiere algun costado original.
* No necesita ser 'innovador', acotado en el tiempo, idealmente 1C. 
* No dejar todo para último momento. Informe de avance tiene que estar resuelto.
* No examen. Pero se aprueba con TP finalizado, o plan de avance.

Mixto en virtualidad/presencialidad (Extensible a todas las materias).

## Objetivo

El objetivo de taller I es acompañar el proceso de crear el TP de especialización.

## Orientaciones

Formulario con pregunta. Para definir orientacion y con eso forman grupos.

* Con _Ricardo_. Todo virtual, desatendido. Encuentros virtuales por skype.
	* Debe apuntar a la realidad. No un ejercicio especulativo, de aplicacion de algoritmos.
	* Mandarle un mail directamente (rmaronna@retina.ar)
	* Ricardo especialista en estadística
	* Parece bien asincrónica la onda, medio como fronton. Uno le tira la pelota, y el te tira algun tip. Algun mano a mano. 
	* Observación: Se retiró tempranito, 20:16.
* Con _Guillermo, Jazmin y Gustavo_
	* Se va a dividir en grupos de a 12 (De entrada somo 68 alumnos en total)
	* Un grupo al mes va a clases presencial. Por fuera de eso, mail y algún otro canal.
	* Tiene hitos, como que ir avanzando en cada encuentro con algo del TP
		* Temas
		* Intro
		* Background, contexto
		* Otros temas que estén relacionados 

Orientaciones generales:
* No investigación metodológica. Dataset real, problema real. 
* Lenguaje de preferencia? No

## Tema

Intención: 

- Conectar:
	* Twitter
	* Grafos
	* Kmeans
	* T-SNE
	* Canasta de items
- Informacion de politica brasileña (Preparar para tesis 2)

Drive para guardar la info: https://drive.google.com/drive/folders/1y9merJmYT7Hf6J9DIlZEjETOS_OMSzEs

## Temario de la materia

Ref: http://datamining.dc.uba.ar/datamining/index.php/academico/materias/86-academico/292-talleredetesis1


### Temario:

• Objetivos y conceptos básicos
• Etapas en la preparación de documentos
• Metodología
• Hallazgos o Resultados
• Tablas
• Figuras
• Introducción, marco teórico y objetivos
• Discusión y conclusiones
• Referencias bibliográficas
• Título y palabras clave
• Resumen y Agradecimientos
• Corrección y apreciación del manuscrito
• Autoría
• Criterios de elección de las revistas. Indización
• Preparación y envío del manuscrito
• Evaluación del manuscrito
• Otros documentos académicos o científicos
• Tesis de Posgrado
• Ética y fraude científico
• Presentaciones orales y en posters

### Actividades conexas

A. Análisis de un artículo
B. Metodología
C. Resultados
D. Tablas
E. Figuras
F. Introducción
G. Discusión y conclusiones
H. Referencias
I. Título y Resumen
J. Apreciación
K. Encuesta
L. Composición
M. Presentación oral

## Grupos

Grupo 1 (primera clase presencial 29 Marzo)
* Sebastián Badaro
* Marina Radice
* Franco Catania
* Pablo Perez
* Gabriel Figueiro
* Fernando Antoni
* Claudia Roxana Aguirre

**Grupo 2 (primera clase presencial 5 Abril)**
* MARIA ROSA LAURIA
* Natalia Argento
* Javier GÓmez
* Lautaro Gabriel Fernández
* Lucas dattoli
* Tatiana badaracco
* Julian Kligman
* Alejandra Fusco
* Maria Jose Ferreyra Villanueva
* Ayelén Opazo
* **Lucas Bertolini**
* Tomas ryan
* Joaquín Aramendía
* Martín Mehaudy
* Valentin Veronesi
* Ricardo villalba

Grupo 3 (primera clase presencial 12 Abril)
* Carolina Daza
* Javier Gamboa
* Mauro Glucini
* Dario Hruszecki
* Sergio Marchio
* Cintia leon
* Leandro Rivero Gonzalez
* Flavia Felicioni
* Matias Gentile
* Federico Rabinovich
* Juan Jose juanjose_if3@...
* Lucas gnocchi
* Lucía Montes Rego
* Malena Álvarez Brito

Grupo 4 (primera clase presencial 19 Abril)
* Ignacio iglesia
* Federico balzarotti
* Mateo Suster
* Luciana Quarracino
* Gisel Trebotic
* Fernando Otálora
* Leandro ariel coria
* G. Sebastián Pedersen
* Julián Ignacio Iannone
* Claudio Collado
* Tomás Marcos
* Federico Scenna
* Federico Moreno
* Nicolás Gianni
* Javier Duque Rodas

## Ultimas clases

Nos vamos acercando al final del cuatrimestre y queríamos contarles (1) cómo sigue el cronograma, (2) los criterios de aprobación que vamos a considerar y (3) fecha de entrega del trabajo del taller:

**1) CRONOGRAMA:**

* 14/06 - Grupo 4
* 21/06 - Grupos 1 y 2
* 28/06 - Grupos 3 y 4
* 05/07 - Todos juntos, pastelitos de membrillo, festejos!

Cómo  no vamos a llegar a hacer una iteración más por grupo decidimos unificar las últimas clases. La idea es que traigan todo lo más avanzado posible para tener una última ronda de feedback. Igual, recuerden que pueden consultar por mail!

**2) CRITERIOS DE APROBACIÓN:**

Para finalizar el taller esperamos COMO MÍNIMO un documento (en el editor de su preferencia) que contenga:

- Título
- Autor
- Resumen (>= 200 pals)
- Introducción
    - Motivación (>= 1/2 carilla)
    - Antecedentes (>= 1 carilla)
    - Objetivos (>= 1/2 carilla)
- Materiales y Métodos
    - Datos (>= 1 carilla): Descripción de los datos. Es obligatorio citar las fuentes.
    - Metodología (>= 2 carillas): Describir los métodos utilizados / a utilizar. Es obligatorio agregar un párrafo especificando a qué materias se relacionan.
- Resultados (>= 3 carillas): Pueden ser preliminares. Es obligatorio tener algo hecho.
- Conclusiones (>= 1 carilla): Pueden ser preliminares. Si no hay mucho hecho se pueden discutir las dificultades a futuro.
- Referencias

**3) ENTREGA DEL TRABAJO:**

Tienen **2 semanas desde que termina el taller** para entregar este trabajo. Es decir, esperamos los trabajos finales del taller (NO de la especialización) para el **19 de julio**.

Lo pueden subir al Drive en pdf / word con su apellido como nombre del archivo.

Recuerden que pueden ir subiendo las cosas a las [carpetas de Drive](https://drive.google.com/drive/folders/1IyffrPCLwke0wXXMHPifsvZ70RK-dED5?usp=sharing) que les compartimos y que nos encuentran por Discord o por mail. Adjuntamos los criterios de aprobación del taller junto con los del trabajo de especialización.


## Link codigo interesantes

* Kmeans: https://www.aprendemachinelearning.com/k-means-en-python-paso-a-paso/
* Text clustering: https://medium.com/text-clustering-with-k-means-in-an-automated/text-clustering-with-k-means-in-an-automated-approach-3302b48f98b6